/* Copyright 2018 sharklaser authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Package sharklaser provides an interface to the Guerrilla Mail API.
Client applications can use it to obtain temporary email addresses and
receive emails. Its name is a reference to a domain name famously
owned by Guerrilla Mail.

API specification can be found here:
https://www.guerrillamail.com/GuerrillaMailAPI.html.

Sharklaser{} exports methods for each API function as well as
higher-level wrapper methods (whose name begins with SL) for some.
Direct API methods do almost no housekeeping and return rather
bare representations of response data, whereas SL* methods simply
return error values and try to do the sensible thing with the returned
data (e.g. storing received email address in Sharklaser.Email and
watching for expiration, saving emails to Sharklaser.Emails). It is
needless to say which set client applications are encouraged to use.

Rather poor usage example:

Create new Sharklaser object:

	shark := sharklaser.New()

Fill in any properties you don't like the default values for:

	shark.UserAgent = "SuperFancyClient -=iCeCoLdMoD=-"
	shark.UpdateInt = 120
	shark.DomainName = "sharklasers.com"

Get a temporary address, check for email:

	getaddr_err := shark.SLGetAddr() // Fills in shark.EmailAddr, notices address expiration
	getmail_err := shark.SLUpdate() // Saves them to shark.Emails and updates next update time

Access Sharklaser.Emails:

	fmt.Println(shark.Emails[0].Mail_subject)

Retrieve email body:

	mail_body, fetch_err := shark.SLFetchEmail(0) // Fetches email body if unread, subsequently retrieves it from memory.

Be polite and let the server release the address when you're done:

	shark.ForgetMe()

If WriteDebugLog is set to true and DebugLogFname contains a valid
filename, API requests and responses will be logged to that file.

If DummyMode is set to true, no network requests will be sent.
Instead, a file in directory DummyDir whose name matches the API
function requested will be read. This is useful if you're developing a
client application and do not wish to bother Guerrilla Mail with
constant requests. However, parameters will be stripped and so only
basic API interaction can be emulated this way.

*/
package sharklaser

import (
	"encoding/json"
	"errors"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const (
	APIURL               = "https://api.guerrillamail.com/ajax.php"
	EMAIL_TTL            = int64(3600)
	EMAIL_LIST_MAX       = 20
	HEADER_UA            = "User-Agent"
	HEADER_COOKIE        = "Cookie"
	HEADER_SETCOOKIE     = "Set-Cookie"
	HEADER_CONTENTTYPE   = "Content-Type"
	CONTENTTYPE_POST     = "application/x-www-form-urlencoded"
	PATTERN_SUBS_DELETED = "SUBSCR=deleted"
)

// Error message constants.
const (
	ERROR_PREFIX = "SRKLSR: "
	// get_email_list with offset <1 fetches the automated welcome message,
	// which uses an incompatible version of the API.
	// Better to refuse to fetch it.
	ERROR_EXPIRED       = ERROR_PREFIX + "email address expired"
	ERROR_GETEMAILSEQ   = ERROR_PREFIX + "get_email_list with offset <1 not supported"
	ERROR_MAILNOTINLIST = ERROR_PREFIX + "email ID not found in local list"
	ERROR_EMPTYVALUE    = ERROR_PREFIX + "received an empty value"
	ERROR_FORGETFAILED  = ERROR_PREFIX + "failed to discard address"
)

// Guerillamail API-related constants.
const (
	FUNCTION_GETEMAIL   = "get_email_address"
	FUNCTION_SETEMAIL   = "set_email_user"
	FUNCTION_CHECKEMAIL = "check_email"
	FUNCTION_GETLIST    = "get_email_list"
	FUNCTION_GETOLDER   = "get_older_list"
	FUNCTION_FETCHEMAIL = "fetch_email"
	FUNCTION_FORGETME   = "forget_me"
	FUNCTION_DELEMAIL   = "del_email"
	PARAM_FUNCTION      = "f"
	PARAM_IP            = "ip"
	PARAM_AGENT         = "agent"
	PARAM_LANG          = "lang"
	PARAM_SUBSCR        = "SUBSCR"
	PARAM_EMAILUSER     = "email_user"
	PARAM_SEQ           = "seq"
	PARAM_OFFSET        = "offset"
	PARAM_EMAILID       = "email_id"
	PARAM_EMAILADDR     = "email_addr"
	PARAM_EMAILIDS      = "email_ids[]"
	LABEL_SESSIONCOOKIE = "PHPSESSID"
	LABEL_SUBSCOOKIE    = PARAM_SUBSCR
)

// Language index constants. Currenlty unused.
// const (
// 	LANG_EN = iota
// 	LANG_FR
// 	LANG_NL
// 	LANG_RU
// 	LANG_TR
// 	LANG_UK
// 	LANG_AR
// 	LANG_KO
// 	LANG_JP
// 	LANG_ZH
// 	LANG_ZHHANT
// )

// Language constants. Currently unused.
// var Languages = [...]string{"en", "fr", "nl", "ru", "tr", "uk", "ar", "ko", "jp", "zh", "zh-hant"}

// List of valid Guerrilla Mail domain names.
var DomainNames = [...]string{"sharklasers.com", "guerrillamail.info", "grr.la", "guerrillamail.biz", "guerrillamail.com", "guerrillamail.de", "guerrillamail.net", "guerrillamail.org", "guerrillamailblock.com", "pokemail.net", "spam4.me"}

// API does not require this, but it's polite to use GET or POST
// depending on the effects of your request.
var methods = map[string]string{
	FUNCTION_GETEMAIL:   http.MethodGet,
	FUNCTION_SETEMAIL:   http.MethodPost,
	FUNCTION_CHECKEMAIL: http.MethodGet,
	FUNCTION_GETLIST:    http.MethodGet,
	FUNCTION_GETOLDER:   http.MethodGet,
	FUNCTION_FETCHEMAIL: http.MethodGet,
	FUNCTION_FORGETME:   http.MethodPost,
	FUNCTION_DELEMAIL:   http.MethodPost,
}

// Central point of the sharklaser library.
type Sharklaser struct {
	client *http.Client
	// User-settable preferences.
	UserAgent  string
	UpdateInt  int64
	DomainName string
	// Received from API, or filled in based on API responses.
	CreationTS   int64
	SessionToken string
	SubsToken    string // This is both user- and API-set.
	SubsActive   bool   // Not used.
	EmailAddr    string
	Alias        string
	LatestEmail  int
	AddrActive   bool
	// Expired      bool // Merged with AddrActive.
	// LatestUpdate int64
	NextUpdate int64
	Emails     []Email
	// Debug log: dump raw requests and responses to a file.
	WriteDebugLog bool
	DebugLogFname string
	// Dummy mode: no network interaction, read responses from files.
	DummyMode bool
	DummyDir  string
}

// All the top-level parameters a response from the API should consist of.
type APIResponse struct {
	Email_addr      string
	Email_timestamp int64  // Email address creation time.
	Alias           string // Obfuscated address.
	Sid_token       string // This contains a copy of the PHPSESSID cookie.
	Deleted_ids     []string
	// Subscription stuff:
	S_active       string // Y or N.
	S_date         string
	S_time         int64
	S_time_expires int64
}

// Parameters for each email in a list as provided in reply to check_email.
type Email struct {
	Mail_id        string
	Mail_from      string
	Mail_subject   string // Unescaped by us.
	Mail_excerpt   string // Unescaped by us.
	Mail_timestamp string
	Mail_read      string // 0 or 1.
	Mail_date      string
	Att            string
	Mail_size      string
	Mail_body      string // Unescaped by us (but see note at FetchEmail()).
	// Parameters not in API, added by us:
	FlaggedForDeletion bool
}

// Reply to check_email or get_email_list.
type EmailList struct {
	List      []Email
	Count     string
	Email     string // Watch this variable for changes.
	Alias     string
	Ts        int64 // Email address creation time.
	Sid_token string
	// Stats
	// Auth
}

// apiRequest is the back-end method for all API requests.
// It takes as arguments the function to request (value of 'f' parameter)
// and a url.Values object containing additional request parameters.
// It takes care of sending headers and setting and sending cookies.
func (shark *Sharklaser) apiRequest(function string, vals url.Values) (text []byte, err error) {
	method := methods[function]
	var req_url, vals_enc string
	vals.Set(PARAM_FUNCTION, function)
	vals.Set(PARAM_AGENT, shark.UserAgent)
	vals_enc = vals.Encode()
	if shark.DummyMode { // Dummy mode operation.
		var dummyf *os.File
		fname := path.Join(shark.DummyDir, function)
		dummyf, err = os.Open(fname)
		if err != nil {
			return
		}
		text, err = ioutil.ReadAll(dummyf)
	} else { // Live network operation.
		var req *http.Request
		var res *http.Response
		// Use GET or POST as appropriate for each function.
		if method == http.MethodGet {
			req_url = APIURL + "?" + vals_enc
			req, err = http.NewRequest(method, req_url, nil)
		} else if method == http.MethodPost {
			req_url = APIURL
			req, err = http.NewRequest(method, req_url, strings.NewReader(vals_enc))
		}
		if err != nil {
			return
		}
		req.Header.Add(HEADER_UA, shark.UserAgent)
		req.Header.Add(HEADER_COOKIE, shark.SessionToken)
		if method == http.MethodPost {
			req.Header.Add(HEADER_CONTENTTYPE, CONTENTTYPE_POST)
		}
		if shark.SubsActive {
			req.Header.Add(HEADER_COOKIE, shark.SubsToken)
		}
		res, err = shark.client.Do(req)
		defer res.Body.Close()
		if err != nil {
			return
		}
		for _, cookie := range res.Header[HEADER_SETCOOKIE] {
			if strings.HasPrefix(cookie, LABEL_SESSIONCOOKIE) {
				shark.SessionToken = strings.Split(cookie, ";")[0]
			} else if strings.HasPrefix(cookie, LABEL_SUBSCOOKIE) {
				cookie_filling := strings.Split(cookie, ";")[0]
				if cookie_filling == PATTERN_SUBS_DELETED {
					shark.SubsToken = ""
				} else {
					shark.SubsToken = cookie_filling
				}
			}
		}
		text, err = ioutil.ReadAll(res.Body)
	}
	// Write debug log?
	if shark.WriteDebugLog {
		logf, _ := os.OpenFile(shark.DebugLogFname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644) // Come on, Go, no prettier way to append?
		defer logf.Close()
		log.SetOutput(logf)
		log.Println(function)
		logf.WriteString(req_url + "\n")
		if method == http.MethodPost {
			logf.WriteString(vals_enc + "\n")
		}
		logf.WriteString(string(text) + "\n\n")
	}
	return
}

// Guerrilla Mail API methods.

// GetEmail requests a new email address.
func (shark *Sharklaser) GetEmailAddress() (apiresp *APIResponse, err error) {
	apiresp = new(APIResponse)
	vals := url.Values{}
	if len(shark.SubsToken) > 0 {
		vals.Add(PARAM_SUBSCR, shark.SubsToken)
	}
	res, err := shark.apiRequest(FUNCTION_GETEMAIL, vals)
	if err != nil {
		return
	}
	err = json.Unmarshal(res, apiresp)
	return
}

// SetEmail requests setting a specific email username.
func (shark *Sharklaser) SetEmailUser(user string) (apiresp *APIResponse, err error) {
	apiresp = new(APIResponse)
	vals := url.Values{}
	vals.Add(PARAM_EMAILUSER, user)
	res, err := shark.apiRequest(FUNCTION_SETEMAIL, vals)
	if err != nil {
		return
	}
	err = json.Unmarshal(res, apiresp)
	return
}

// CheckEmail requests the list of emails starting at sequence number seq.
// Since the API for it is buggy and GetEmailList basically does the
// same thing, it is recommended to use that other method instead.
func (shark *Sharklaser) CheckEmail(seq int) (emaillist *EmailList, err error) {
	emaillist = new(EmailList)
	vals := url.Values{}
	vals.Add(PARAM_SEQ, strconv.Itoa(seq))
	res, err := shark.apiRequest(FUNCTION_CHECKEMAIL, vals)
	if err != nil {
		return
	}
	if err = json.Unmarshal(res, emaillist); err != nil {
		return
	}
	for i := range emaillist.List {
		emaillist.List[i].Mail_subject = html.UnescapeString(emaillist.List[i].Mail_subject)
		emaillist.List[i].Mail_excerpt = html.UnescapeString(emaillist.List[i].Mail_excerpt)
	}
	return
}

// GetEmailList requests the list of emails starting at sequence number seq.
// To avoid fetching the automated welcome message, which breaks the API,
// we disallow an offset lesser than 1.
func (shark *Sharklaser) GetEmailList(offset, seq int) (emaillist *EmailList, err error) {
	emaillist = new(EmailList)
	vals := url.Values{}
	if seq < 1 {
		err = errors.New(ERROR_GETEMAILSEQ)
	}
	vals.Add(PARAM_OFFSET, strconv.Itoa(offset))
	vals.Add(PARAM_SEQ, strconv.Itoa(seq))
	res, err := shark.apiRequest(FUNCTION_GETLIST, vals)
	if err != nil {
		return
	}
	if err = json.Unmarshal(res, emaillist); err != nil {
		return
	}
	for i := range emaillist.List {
		emaillist.List[i].Mail_subject = html.UnescapeString(emaillist.List[i].Mail_subject)
		emaillist.List[i].Mail_excerpt = html.UnescapeString(emaillist.List[i].Mail_excerpt)
	}
	return
}

// FetchEmail downloads an email and returns its unescaped body.
// Mail_body is missing from get_email_list replies but present in
// fetch_email replies. FetchEmail() therefore constructs a new Email
// object intependent of any that may exist in Sharklaser.Emails, and
// simply returns its unescaped Mail_body.
func (shark *Sharklaser) FetchEmail(emailid string) (email *Email, err error) {
	email = new(Email)
	vals := url.Values{}
	vals.Add(PARAM_EMAILID, emailid)
	res, err := shark.apiRequest(FUNCTION_FETCHEMAIL, vals)
	if err != nil {
		return
	}
	if err = json.Unmarshal(res, email); err != nil {
		return
	}
	email.Mail_body = html.UnescapeString(string(email.Mail_body))
	return
}

// ForgetMe requests that the server forget the current email address.
func (shark *Sharklaser) ForgetMe() (success bool, err error) {
	vals := url.Values{}
	text, err := shark.apiRequest(FUNCTION_FORGETME, vals)
	if string(text) == "true" {
		success = true
	} else {
		success = false
	}
	return
}

// DelEmail deletes a list of emails from the server.
func (shark *Sharklaser) DelEmail(email_ids []string) (deleted_ids []string, err error) {
	apiresp := new(APIResponse)
	vals := url.Values{}
	for _, id := range email_ids {
		vals.Add(PARAM_EMAILIDS, id)
	}
	res, err := shark.apiRequest(FUNCTION_DELEMAIL, vals)
	if err != nil {
		return
	}
	err = json.Unmarshal(res, apiresp)
	if err == nil {
		deleted_ids = apiresp.Deleted_ids
	}
	return
}

// Utility methods.

// updateEmailAddress updates Sharklaser.EmailAddr and Sharklaser.Alias.
// It appends a domain name to alias, which is returned as a bare
// username by the API, plus it can set a different domain name than
// handed out by the API for both addresses if a valid one was
// provided to Sharklaser. This is done locally, as all Guerrilla Mail
// domains are equivalent and deliver to the same account.
func (shark *Sharklaser) updateEmailAddress(address, alias string) {
	username := strings.SplitN(address, "@", 2)[0]
	domain := strings.SplitN(address, "@", 2)[1]
	for _, d := range DomainNames {
		if d == shark.DomainName {
			domain = shark.DomainName
			break
		}
	}
	shark.EmailAddr = username + "@" + domain
	shark.Alias = alias + "@" + domain

}

// Higher-level wrapper methods.

// SLGetAddr is a wrapper around Sharklaser.GetEmailaddress().
func (shark *Sharklaser) SLGetAddr() (err error) {
	var apiresp *APIResponse
	apiresp, err = shark.GetEmailAddress()
	if err != nil {
		return
	}
	shark.updateEmailAddress(apiresp.Email_addr, apiresp.Alias)
	shark.CreationTS = apiresp.Email_timestamp
	shark.AddrActive = true
	return
}

// SLSetUser is a wrapper around Sharklaser.SetEmailUser().
func (shark *Sharklaser) SLSetUser(user string) (err error) {
	var apiresp *APIResponse
	if len(user) == 0 {
		err = errors.New(ERROR_EMPTYVALUE)
		return
	}
	apiresp, err = shark.SetEmailUser(user)
	if err != nil {
		return
	}
	shark.updateEmailAddress(apiresp.Email_addr, apiresp.Alias)
	shark.CreationTS = apiresp.Email_timestamp
	return
}

// SLUpdate is a wrapper around Sharklaser.GetEmaillist().
// It takes care of requesting only new emails, saving them in
// Sharklaser.Emails and calculating the time when the next automatic
// update is due.
func (shark *Sharklaser) SLUpdate() (err error) {
	var seq, count int
	if shark.LatestEmail != 0 {
		seq = shark.LatestEmail
	} else {
		seq = 1
	}
	list_res, err := shark.GetEmailList(0, seq)
	if err == nil { // Don't call it expired just because the API request was dropped!
		if list_res.Email == "" {
			shark.AddrActive = false
			err = errors.New(ERROR_EXPIRED)
			// } else {
			// 	shark.Expired = false
		}
	}
	if len(list_res.List) > 0 {
		shark.LatestEmail, err = strconv.Atoi(list_res.List[len(list_res.List)-1].Mail_id)
		shark.Emails = append(shark.Emails, list_res.List...)
	}
	// shark.LatestUpdate = time.Now().Unix()
	shark.NextUpdate = time.Now().Unix() + shark.UpdateInt
	// If by any chance we got the maximum number of emails per request,
	// we check again in case there are more.
	count, _ = strconv.Atoi(list_res.Count)
	if count == EMAIL_LIST_MAX {
		err = shark.SLUpdate()
	}
	return
}

// SLFetchEmail is a wrapper around Sharlaser.FetchEmail().
// It takes care of requesting and storing an email's body only if it
// hasn't been already retrieved.
func (shark *Sharklaser) SLFetchEmail(index int) (body string, err error) {
	var email *Email
	if index < 0 || index >= len(shark.Emails) {
		err = errors.New(ERROR_MAILNOTINLIST)
		return
	}
	if shark.Emails[index].Mail_read == "0" {
		email, err = shark.FetchEmail(shark.Emails[index].Mail_id)
		if err == nil {
			shark.Emails[index].Mail_body = email.Mail_body
			shark.Emails[index].Mail_read = "1"
		}
	}
	body = shark.Emails[index].Mail_body
	return
}

// SLForgetMe is a wrapper around Sharklaser.ForgetMe().
func (shark *Sharklaser) SLForgetMe() (err error) {
	var success bool
	success, err = shark.ForgetMe()
	if success {
		shark.AddrActive = false
	} else if err == nil {
		err = errors.New(ERROR_FORGETFAILED)
	}
	return
}

// SLDelEmail is a wrapper around Sharklaser.DelEmail().
// It takes care of deleting from the local email list the messages
// that the API reports as successfully deleted.
func (shark *Sharklaser) SLDelEmail() (err error) {
	var ids, deleted_ids []string
	for email := range shark.Emails {
		if shark.Emails[email].FlaggedForDeletion {
			ids = append(ids, shark.Emails[email].Mail_id)
		}
	}
	deleted_ids, err = shark.DelEmail(ids)
	if err != nil {
		return
	}
	purged_list := make([]Email, 0, len(shark.Emails))
	for _, email := range shark.Emails {
		found := false
		for _, id := range deleted_ids {
			if email.Mail_id == id {
				found = true
			}
		}
		if found == false {
			purged_list = append(purged_list, email)
		}
	}
	shark.Emails = purged_list
	return
}

// SLExtend requests an extension of the address' lifetime. The extend
// API function is disabled, so this is actually a wrapper around
// Sharklaser.SetEmailUser() instead.
func (shark *Sharklaser) SLExtend() (err error) {
	username := strings.Split(shark.EmailAddr, "@")[0]
	err = shark.SLSetUser(username)
	return
}

// SLRenew initiates a new session by requesting a new address and
// then promptly switches to the previous address, in practise
// extending an address' lifetime beyond its expiration.
func (shark *Sharklaser) SLRenew() (err error) {
	username := strings.Split(shark.EmailAddr, "@")[0]
	err = shark.SLGetAddr()
	if err == nil {
		err = shark.SLSetUser(username)
	}
	return
}

// Reckon* methods provide local-side guesses of current status.

// ReckonTimeLeft calculates time left before account expires based on
// creation timestamp and current time.
func (shark *Sharklaser) ReckonTimeLeft() int64 {
	return EMAIL_TTL - (time.Now().Unix() - shark.CreationTS)
}

// New sets up a new Sharklaser object and returns a pointer to it.
func New() *Sharklaser {
	client := &http.Client{}
	shark := Sharklaser{
		client:        client,
		UserAgent:     "sharklaser",
		DebugLogFname: "sharklaser.log",
		UpdateInt:     60,
	}
	return &shark
}
