Sharklaser is a [golang][go] library for interfacing with the
[Guerrilla Mail][gm] disposable e-mail service [API][]. It is named
after one of Guerrilla Mail's most memorable domain names.

[go]:		https://golang.org/
[gm]:		https://www.guerrillamail.com/
[api]:	https://www.guerrillamail.com/GuerrillaMailAPI.html

# Obtaining it #

Once you've [set up go](https://golang.org/doc/install), this should do it:

	go get -u gitlab.com/lonvale/sharklaser

# Using it #

Usage is centred around the Sharklaser type:

	shark := sharklaser.New() // Initialise.
	getaddr_err := shark.SLGetAddr() // Obtain a disposable address (accessible at shark.EmailAddr).
	getmail_err := shark.SLUpdate() // Check for e-mail (saved to shark.Emails)
	mail_body, fetch_err := shark.SLFetchEmail(0) // Retrieve body for e-mail at index 0.
	shark.ForgetMe() // Discard address.

For a usage example, the brave-hearted can try to make sense of our
very own [grrla](https://gitlab.com/lonvale/grrla) text-mode client.

API is not to be considered stable at this point. Changes in function
signatures and whatnot are likely in the next stages of
development. Exercise caution.

# License #

Sharklaser is [Free Software][freesw] made available under the terms
of the GNU General Public License, version 3 or later.

[freesw]: https://www.gnu.org/philosophy/free-sw.html
